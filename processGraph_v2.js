/**
 * 全局变量
 */
// 当前环节信息
var _currentAct = null;
// 所有节点数据
var _nodeData = null;
// 流程审批信息
var approveInfo = null;
// 流程状态
var flowState = 1;
// 流程数据
var lineData = {};
// 流程总列数
var columnTotal = 0;
// 流程总行数
var rowTotal = 0;


// 窗口缩放监听
var resizeTimer = null;
$(window).bind('resize', function (){
	if (resizeTimer) clearTimeout(resizeTimer);
	resizeTimer = setTimeout(function(){
		// 加载新版流程图
		initFlowChart(_currentAct,_nodeData);
	} , 500);
});

/**
 * 加载新版流程图
 * @param data
 */
function loadNewGraph(){
	// 获取流程审批信息
	getApproveByFlow(function(res){
		approveInfo = res.participants;
		// 获取当前环节
		getCurrentAct(function(currentAct){
			_currentAct = currentAct;
			getNodeData(function(nodeData){
				_nodeData = nodeData;
				initFlowChart(_currentAct,_nodeData);
			});
		});
	});
}

/**
 * 初始化流程图，数据为全局变量，避免重复加载
 * @param currentAct 环节信息
 * @param nodeData 节点信息
 */
function initFlowChart(currentAct,nodeData){
	// 流程状态
	flowState = 1;
	// 流程数据
	lineData = {};
	// 流程总列数
	columnTotal = 0;
	// 流程总行数
	rowTotal = 0;
	// 节点数
	var datas = assemblyDataByFlow(currentAct,nodeData);
	// 有需要预测的数据
	if(datas.length > 0){
		// 追加结束节点
		datas.push({'actDefName':'结束','runState':3});
		// 每行多少个
		columnTotal = Math.floor((window.innerWidth-240)/208) - 1;
		// 如果每行为0个，最低改为每行1个
		columnTotal = columnTotal==0?1:columnTotal;
		// 计算行数，区分奇偶行
		for(var i = 0; i < datas.length; i++){
			if (i%columnTotal == 0) {
				rowTotal++;
				if(lineData[rowTotal] == null){
					lineData[rowTotal] = []; 
				}
			}
			lineData[rowTotal].push(datas[i]);
		}
		// 渲染模板
		setFlowChartHtml(lineData,columnTotal,rowTotal,approveInfo);
	}
}

/**
 * 获取流程环节信息
 * @param processInstID
 */
function getNodeData(success){
	var messageid = nui.loading("loading..."); 
	$.ajax({
		url:contextPath+"/com.sfy.oa.bizform.BizForm.getActivityParticipant.biz.ext",
		type:'POST',
		data: nui.encode({processInstId:processInstID}),
		contentType: 'text/json',
		success: function (res){
			nui.hideMessageBox(messageid);
			if (res.exception) {
				nui.alert(res.exception.message);
			}else{
				success(res);
			}
		}
	});
}

/**
 * 获取下一环节信息
 */
function getCurrentAct(success){
	var messageid = nui.loading("loading..."); 
	$.ajax({
		url:contextPath+"/com.sfy.oa.bizform.BizForm.getNextActivityParticipant.biz.ext",
		type:'POST',
		data: nui.encode({processInstID:processInstID}),
		contentType: 'text/json',
		success: function (res){
			nui.hideMessageBox(messageid);
			if (res.exception) {
				nui.alert(res.exception.message);
			}else{
				var currentAct = {};
				currentAct.processInstState = res.processInstState;
				currentAct.actInstList = getActInstList(res.actInstList);
				currentAct.participantList = getParticipantList(res.participantList);
				success(currentAct);
			}
		}
	});
}

/**
 * 获取流程审批信息
 */
function getApproveByFlow(success){
	var messageid = nui.loading("loading..."); 
	nui.ajax({
		url: bootPATH + "../../rest/services/bps/webcontrol/getProcessGraph",
		type: "POST",
		data: { procInstID:processInstID,
				procDefID:'',
				procDefName:''
			},
		success: function (data) {
			nui.hideMessageBox(messageid);
			success(data.result);
		}
	});
}

/**
 * 模板的生成
 * @param lineData 行数据
 * @param columnTotal 总列数
 * @param rowTotal 总行数
 * @param approveInfo 审批节点信息
 */		
function setFlowChartHtml(lineData,columnTotal,rowTotal,approveInfo) {
	// 克隆数据
	var flowData = nui.clone(lineData);
	// 清空html代码
	$('#flow-chart').empty();
	// 开始节点
	var html = $("#flow-chart-start").html();
	// 中间节点
	html += '<div class="flow-chart-body">';
	// 组装流程图
	for(var i in flowData){
		// 赋值多少行
		var arr = flowData[i];
		// 当前第几行
		var rowIndex = Number(i);
		// 是否偶数行
		var isEven = rowIndex%2 == 0;
		// 偶数行倒序
		if(isEven){
			arr = arr.reverse();
		}
		// 开始奇(odd)偶(even)行
		html += '<div class="flow-row-'+(isEven?"even":"odd")+'">';
		// 偶数行且最后一行不足个数时，需要补占位
		if (isEven && arr.length != columnTotal && rowIndex == rowTotal) {
			for(var k = 0; k < (columnTotal - (arr.length)); k++){
				html += '<div class="item-node" style="visibility: hidden;"></div>';
				html += '<div style="visibility: hidden;width:93px;height:76px;"></div>';
			}
		}
		// 渲染节点
		for(var j = 0 ; j < arr.length ; j++){
			// 结束节点
			if(arr[j].actDefName == '结束'){
				// 赋值结束html
				html += $("#flow-chart-end").html().replace(/{flowState}/g,flowState==7?'7_0':'1');
				html += getConnect(rowTotal,isEven,rowIndex,arr[j],j,arr.length);
			}else{
				// 流转节点
				html += $("#flow-chart-body").html()
					.replace(/{runState}/g,arr[j].runState)
					.replace(/{nodeName}/g,getCutting(arr[j].actDefName,true,10))
					.replace(/{nodeNameDetail}/g,getCutting(arr[j].actDefName,false,0))
					.replace(/{approver}/g,getApprover(arr[j],approveInfo,true))
					.replace(/{approverDetail}/g,getApprover(arr[j],approveInfo,false))
					.replace(/{nodeId}/g,arr[j].actDefId)
					.replace(/{imgName}/g,getIcon(arr[j].runState))
					.replace(/{detailTr}/g,getDetailTr(arr[j],approveInfo));
				// 增加连线
				html += getConnect(rowTotal,isEven,rowIndex,arr[j],j,arr.length);
			}
		}
		// 结束奇偶行
		html += '</div>';
	}
	// 结束流程图
	html += '</div>';
	
	// 赋值html代码
	$('#flow-chart').append(html);
	// 设置节点样式
	setFlowNodeStyle(flowData);
	// 设置流程图居中
	setFlowChartCenter(columnTotal);
}

/**
 * 设置流程图居中 
 * @param numTotal
 * @returns
 */
function setFlowChartCenter(columnTotal){
	var flowChartWidth = columnTotal * 125 + (columnTotal - 1) * 93 + 125;
	var blankWidth = window.innerWidth - flowChartWidth;
	$('.flow-chart-start').css({'margin-left':blankWidth/2>30?blankWidth/2-30:blankWidth/2});
	$('.container').css({'height':'auto'});
	$('.container').css({'height':($('.container').height()+20)+'px'});
}

/**
 * 获取当前环节，多个环节用逗号区分
 * @param data
 */
function getActInstList(data){
	var activityDefIDs = [];
	var activityInstNames = [];
	for(var i = 0 ; i < data.length ; i++){
		activityDefIDs.push(data[i].activityDefID);
		activityInstNames.push(data[i].activityInstName);
	}
	return {'activityDefIDs':activityDefIDs.join(','),'activityInstNames':activityInstNames.join(',')};
}

/**
 * 获取当前处理人，多个人用逗号区分
 * @param data
 */
function getParticipantList(data){
	var empInfo = [];
	for(var i = 0 ; i < data.length ; i++){
		empInfo.push(data[i].id + '|' + data[i].name)
	}
	return {'empInfo':empInfo.join(',')};	
}

/**
 * 组装数据
 * @param currentAct
 * @param nodeData
 */
function assemblyDataByFlow(currentAct,nodeData){
	var graphDatas = [];
	var nodes = nodeData.data;
	// 流程状态
	flowState = currentAct.processInstState;
	// 1:已完成；2：运行中；3：未启动
	var runState = 1;
	for(var i = 0 ; i < nodes.length; i++){
		// 赋值当前环节状态
		if(checkIndexOf(currentAct.actInstList.activityDefIDs,nodes[i].actDefId) && runState != 3){
			nodes[i].runState = 2;
			runState = 3;
		}else{
			nodes[i].runState = runState;
		}
		// 多工作项
		if(nodes[i].isMulWIValid){
			graphDatas = getMulWiValidData(nodes[i],currentAct,graphDatas,nodeData.dataDetail);
		}else{
			graphDatas.push(nodes[i]);
		}
	}
	return graphDatas;
}

/**
 * 多工作项单独处理
 * @param node
 * @param currentAct
 */
function getMulWiValidData(node,currentAct,graphDatas,historyDatas){
	// 运行状态 1:已完成；2：运行中；3：未启动
	var runState = 1;
	// 多工作项环节处理人
	var participants = node.participant.split(',');
	// 是否顺序审批
	var isSequentialExecute = node.isSequentialExecute;
	// 当前处理人
	var empInfo = currentAct.participantList.empInfo;
	//  非顺序审批
	var notSeqData = [];
	// 组装数据
	for(var p = 0 ; p < participants.length ; p++){
		var itemMul = nui.clone(node);
		// 赋值多工作项处理人
		itemMul.participant = participants[p];
		// 多工作项且运行中
		if(itemMul.runState == 2){
			// 顺序审批,或者当前处理人数与环节处理人数相等
			if(isSequentialExecute || empInfo.split(',').length == participants.length){
					// 重新赋值运行状态
					itemMul.runState = runState;
					if(empInfo.indexOf(itemMul.participant) != -1){
						itemMul.runState = 2;
						runState = 3;
					}
				graphDatas.push(itemMul);
			}else{ // 非顺序审批,或者当前处理人数与环节处理人数不相等
				notSeqData.push(itemMul);
			}
		}else{
			graphDatas.push(itemMul);
		}
	}
	// 处理非顺序审批数据
	if(notSeqData.length > 0){
		graphDatas = pushArrAll(graphDatas,notSeqDataBySort(notSeqData,historyDatas));
	}
	return graphDatas;
}

// 处理非顺序审批数据
function notSeqDataBySort(notSeqData,historyDatas){
	var historyNotSeq = [];
	var runNotSeq = [];
	var historyData = historyDatas[historyDatas.length - 1];
	for(var n = 0 ; n < notSeqData.length ; n++){
		var participantId = notSeqData[n].participant.split('|')[0];
		if(historyData.toPersons && historyData.toPersons.indexOf(participantId) == -1){
			notSeqData[n].runState = 1;
			historyNotSeq.push(notSeqData[n]);
		}else{
			runNotSeq.push(notSeqData[n]);
		}
	}
	return historyNotSeq.concat(runNotSeq);
}

/**
 * 获取流程处理人
 * @param e 当前节点信息
 * @param approveInfo 节点审批信息
 * @param isCutting 是否超出10字符为省略号
 * @returns {String} 当前审批人
 */
function getApprover(e,approveInfo,isCutting){
	var participant = e.participant;
	var resultApprover = '';
	// 已完成节点审批人
	if(e.runState == 1 && approveInfo[e.actDefId].length > 0 /*&& !e.isMulWIValid*/){
		var infos = approveInfo[e.actDefId];
		var emps = [];
		for(var f = 0 ; f < infos.length ; f++){
			emps.push(infos[f].participant);
		}
		if(emps.length > 0){
			resultApprover = emps.join(',');
		}
	}else if(!checkNullOrEmpty(participant)){
		// 预测节点审批人
		var approvers = [];
		var emps = participant.split(',');
		for(var i = 0 ; i < emps.length ; i++){
			approvers.push(emps[i].split('|')[1]);
		}
		resultApprover = approvers.join(',');
	}else{
		resultApprover = '待定';
	}
	// 审批人过长截取
	if(isCutting && resultApprover && resultApprover.length > 10){
		resultApprover = resultApprover.substring(0,10) + '...';
	}
	return resultApprover;
}

/**
 * 获得截取文字
 * @param name 需截取内容
 * @param isCutting 是否截取
 * @param length 截取长度
 */
function getCutting(name,isCutting,length){
	if(isCutting && name.length > length){
		name = name.substring(0,length)+'...';
	}
	return name;
}

// 获取流程icon
function getIcon(e){
	var iconName = '';
	switch(e){
	    case 1:
	    	iconName = 'finished';
	        break;
	    case 2:
	    	iconName = 'run';
	    	break;
	    case 3:
	    	iconName = 'forecast';
	        break;
	    default:
	    	iconName = 'run';
	}
	return iconName;
}

// 设置连线
function getConnect(rowTotal,isEven,rowIndex,rowData,columnIndex,columnLength){
	var resultHtml = '';
	var xHtml = '';
	var yHtml = '';
	// 偶数行的第一个，并且当前行不是最后一行
	if(isEven && columnIndex == 0 && rowIndex != rowTotal){
		yHtml = $("#flow-chart-connect").html();
		yHtml = yHtml.replace(/{xy}/g,'y');
	}
	// 奇数行的最后一个，并且当前行不是最后一行
	if(!isEven && columnIndex == columnLength - 1 && rowIndex != rowTotal){
		yHtml = $("#flow-chart-connect").html();
		yHtml = yHtml.replace(/{xy}/g,'y');
	}
	// 不是当前行的最后1个
	if(columnIndex != columnLength - 1){
		xHtml = $("#flow-chart-connect").html();
		xHtml = xHtml.replace(/{xy}/g,'x');
	}
	resultHtml = yHtml + xHtml;
	// 连线颜色、悬浮提示
	resultHtml = resultHtml.replace(/{runState}/g,(rowData.runState==3?3:1))
						   .replace(/{branchRule}/g,(checkNullOrEmpty(rowData.branchRule)?'默认':rowData.branchRule));
	return resultHtml;
}

// 设置节点样式
function setFlowNodeStyle(flowData){
	// 结束节点行只有它自己时
	if(flowData[rowTotal].length == 1){
		$('.item-nonde-end').css({'top': '0%','right': '50%','transform':'translate(50%,0%)'});
	}
}

/**
 * 鼠标悬浮显示
 * @param el
 */
function showParticipantDetail(el){
	// 节点
	var defaultWidth = 125;
	var defaultHeight = 75;
	// 节点
	// 节点明细
	var oDiv = $(el).find('.item-node-detail');
	// 审批人
	var approver = $(el).find('.item-node-approver').text().trim();
	if(approver == '待定'){
		oDiv.empty();
		oDiv.append('<span style="color:grey;">条件不足，无法预测</span>');
	}
	// 明细div的宽度
	var oWidth = oDiv.width();
	// 明细div的高度
	var oHeight = oDiv.height();
	// 网页可见区域宽(包括边线的宽)
	var offsetWidth = document.body.offsetWidth;
	// 网页可见区域宽(包括边线的宽)
	var offsetHeight = document.body.offsetHeight ;
	// 隐藏明细存在
	if (oDiv) {
		// 预测显示
		if(approver != '待定'){
			// 右边位置不足
			if ((offsetWidth - pageX) <= oWidth) {
				oDiv.css({'left':-(oWidth-45) + 'px'});
			}
			// 下边位置不足
			if ((offsetHeight - pageY) <= oHeight) {
				oDiv.css({'top':-(oHeight-10) + 'px'});
			}
		}else{
			// 无法预测显示
			oDiv.css({'left':'10px'});
			oDiv.css({'top':'82px'});
		}
		// 显示
		oDiv.show();
	}
}

/**
 * 鼠标离开隐藏
 * @param el
 */
function hideParticipantDetail(el){
	$(el).find('.item-node-detail').hide();
}

var pageY = 0;
var pageX = 0;
//获取鼠标位置
$(document).mousemove(function(e){
	pageX = e.pageX; //鼠标距左顶部的距离
	pageY = e.pageY; //鼠标距上顶部的距离
});

/**
 * 渲染参与者明细
 * @param nodeData 节点信息
 * @param approveInfo 参与者信息	
 */
function getDetailTr(nodeData,approveInfo){
	var resultHtml = '';
	if(nodeData.runState == 1){
		var empDetails = approveInfo[nodeData.actDefId];
		for(var e = 0 ; e < empDetails.length ; e++){
			// 非多工作项
			if(!nodeData.isMulWIValid){
				resultHtml += replaceDetailTr(empDetails[e].participant,empDetails[e].startTime,empDetails[e].endTime,'已审批');
			}else{
				// 多工作项
				if(nodeData.participant.toUpperCase().indexOf(empDetails[e].participant.toUpperCase()) != -1){
					resultHtml += replaceDetailTr(empDetails[e].participant,empDetails[e].startTime,empDetails[e].endTime,'已审批');
					break;
				}else if(nodeData.runState == 1){
					resultHtml += replaceDetailTr(empDetails[e].participant,empDetails[e].startTime,empDetails[e].endTime,'已审批');
					break;
				}else{
					
				}
			}
		}
	}else if(nodeData.runState == 2){
		var empDetails = nodeData.participant.split(',');
		if(!checkNullOrEmpty(empDetails)){
			for(var e = 0 ; e < empDetails.length ; e++){
				var empName = empDetails[e].split('|')[1];
				// 非多工作项
				if(!nodeData.isMulWIValid){
					resultHtml += replaceDetailTr(empName,'-','-','待审批');
				}else{
					// 多工作项
					if(nodeData.participant.toUpperCase().indexOf(empName.toUpperCase()) != -1){
						resultHtml += replaceDetailTr(empName,'-','-','待审批');
						break;
					}
				}
			}	
		}else{
			resultHtml += replaceDetailTr('-','-','-','-');
		}
	}else{
		var empInfos = nodeData.participant;
		if(!checkNullOrEmpty(empInfos)){
			var empDetails = empInfos.split(',');
			for(var e = 0 ; e < empDetails.length ; e++){
				resultHtml += replaceDetailTr(empDetails[e].split('|')[1],'-','-','-');
			}
		}else{
			resultHtml += replaceDetailTr('-','-','-','-');
		}
	}
	return resultHtml;
}

/**
 * 替换参与者html
 * @param participant
 * @param startTime
 * @param endTime
 * @param state
 * @returns {String}
 */
function replaceDetailTr(participant,startTime,endTime,state){
	var templateTr = '<tr class="detail-table-body"><td>{参与者}</td><td>{开始时间}</td><td>{结束时间}</td><td>{状态}</td></tr>';
	templateTr = templateTr.replace(/{参与者}/g,checkNullOrEmpty(participant)?'-':participant)
				  .replace(/{开始时间}/g,checkNullOrEmpty(startTime)?'-':startTime)
				  .replace(/{结束时间}/g,checkNullOrEmpty(endTime)?'-':endTime)
				  .replace(/{状态}/g,checkNullOrEmpty(state)?'-':state);
	return templateTr;
}

/**
 * 判断当前环节是否正在进行中
 * @param activityDefIDs 当前环节
 * @param actDefId 节点中的环节
 */
function checkIndexOf(activityDefIDs,actDefId){
	var isIndex = false;
	var defIds = activityDefIDs.split(',');
	for(var d = 0 ; d < defIds.length ; d++){
		if(defIds[d] == actDefId){
			isIndex = true;
			break;
		}
	}
	return isIndex;
}

/**
 * 双击全选
 * @param el 双击对象
 */
function dbclickfun(el){
	var text = $(el)[0];
	if (document.body.createTextRange) {
		var range = document.body.createTextRange();
		range.moveToelText(text);
		range.select();
	} else if (window.getSelection) {
		var selection = window.getSelection();
		var range = document.createRange();
		range.selectNodeContents(text);
		selection.removeAllRanges();
		selection.addRange(range);
		/*if(selection.setBaseAndExtent){
			selection.setBaseAndExtent(text, 0, text, 1);
		}*/
	} else {
		// alert("none");
	}
}